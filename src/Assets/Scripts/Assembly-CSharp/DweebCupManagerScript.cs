using UnityEngine;

public class DweebCupManagerScript : MonoBehaviour
{
	public P2AnteScript TotalSoda;
	public P2AnteScript P2_Ante;
	public GameObject ContinueButton;
	public GameObject BlankCard;
	public GameObject PreMatch;
	public GameObject P1_Ante;
	public GameObject Cards;
	public UILabel Stakes1;
	public UILabel Stakes2;
	public float Timer;
	public int SpawnPosition;
	public int Card;
	public bool DweebCupUpdated;
	public bool FingerDown;
	public bool CardGrown;
	public bool Ante;
	public float Momentum;
	public bool MoveLeft;
	public bool MoveRight;
}
