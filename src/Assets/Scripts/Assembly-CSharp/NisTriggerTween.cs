using UnityEngine;

public class NisTriggerTween : NisComponent
{
	public float playDelaySecs;
	public float completionDelaySecs;
	public float skipPromptDelaySecs;
	public bool reverse;
	public bool waitForClick;
	public GameObject target;
}
