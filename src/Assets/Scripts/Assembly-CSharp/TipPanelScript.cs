using UnityEngine;

public class TipPanelScript : MonoBehaviour
{
	public UILabel HeaderLabel;
	public UILabel MessageLabel;
	public TipContext Context;
}
