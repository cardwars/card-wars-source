using UnityEngine;

public class CWBattleRingSetLabel : MonoBehaviour
{
	public UILabel ringHitLabel;
	public string labelString;
	public Color color;
	public Color effectColor;
}
