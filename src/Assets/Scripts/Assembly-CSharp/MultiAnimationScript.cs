using UnityEngine;

public class MultiAnimationScript : MonoBehaviour
{
	public string startAnimName;
	public string idleAnimName;
	public string clickAnimName;
	public GameObject OutroEventReceiver;
	public string OutroFunction;
	public float StartOutroFrame;
	public float ReverseSpeed;
}
