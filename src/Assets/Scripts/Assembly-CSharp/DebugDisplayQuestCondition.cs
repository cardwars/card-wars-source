using UnityEngine;

public class DebugDisplayQuestCondition : MonoBehaviour
{
	public UILabel qcLabel;
	public UILabel qcNumLabel;
	public GameObject qcObj;
	public GameObject qcParent;
	public GameObject obj;
}
