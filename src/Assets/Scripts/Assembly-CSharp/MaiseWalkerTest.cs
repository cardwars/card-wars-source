using UnityEngine;

public class MaiseWalkerTest : MonoBehaviour
{
	public Renderer Mouth;
	public Texture Mouth_MPandB;
	public Texture Mouth_AltTH;
	public Texture Mouth_OpenSmall;
	public Texture Mouth_AandI;
	public Texture Mouth_O;
	public Texture Mouth_Consonants;
	public Texture Mouth_LandTH;
	public Texture Mouth_AltConsonants;
	public Texture Mouth_WandQ;
	public Texture Mouth_OpenSmall2;
	public bool AudioPlayed;
	public float Timer;
}
