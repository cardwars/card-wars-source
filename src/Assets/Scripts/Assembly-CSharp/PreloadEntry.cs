using System;
using UnityEngine;

[Serializable]
public class PreloadEntry
{
	public string name;
	public GameObject prefab;
	public int count;
}
