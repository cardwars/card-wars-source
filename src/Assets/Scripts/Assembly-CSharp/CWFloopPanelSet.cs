using UnityEngine;

public class CWFloopPanelSet : MonoBehaviour
{
	public GameObject floopDisplayPanel;
	public int playerType;
	public int lane;
	public bool creatureFlag;
	public CWCommandCardSet commandCardSet;
	public GameObject creatureObj;
	public bool floopSetFlag;
	public GameObject lowerFloopDisplayPanel;
}
