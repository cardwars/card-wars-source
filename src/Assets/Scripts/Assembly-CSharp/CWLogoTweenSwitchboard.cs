using UnityEngine;

public class CWLogoTweenSwitchboard : MonoBehaviour
{
	public TweenScale Tween;
	public float ForwardDuration;
	public float ReverseDuration;
	public float StartX;
	public float StartY;
	public float StartZ;
	public float EndX;
	public float EndY;
	public float EndZ;
}
