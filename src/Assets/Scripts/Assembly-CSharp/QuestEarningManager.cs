using UnityEngine;
using System.Collections.Generic;

public class QuestEarningManager : MonoBehaviour
{
	public List<string> earnedCardsName;
	public List<bool> hasCardFlag;
	public List<string> cardHistory;
	public int earnedCoin;
	public int earnedGem;
	public bool dropedThisBattle;
}
