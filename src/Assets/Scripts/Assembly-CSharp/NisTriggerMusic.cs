using UnityEngine;

public class NisTriggerMusic : NisComponent
{
	public float playDelaySecs;
	public bool toggleMusicOnPlay;
	public AudioSource target;
	public bool autoPlay;
}
