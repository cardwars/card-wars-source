using UnityEngine;

public class CreatureBattleTest : MonoBehaviour
{
	public GameObject Creature1;
	public GameObject Creature2;
	public GameObject HuskerKnightP1_1;
	public GameObject HuskerKnightP1_2;
	public GameObject HuskerKnightP1_3;
	public GameObject HuskerKnightP1_4;
	public GameObject HuskerKnightP2_1;
	public GameObject HuskerKnightP2_2;
	public GameObject HuskerKnightP2_3;
	public GameObject HuskerKnightP2_4;
	public GameObject DustCloud;
	public float Position;
	public float Timer;
	public int Lane;
}
