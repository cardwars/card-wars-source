using UnityEngine;

public class OpponentScript : MonoBehaviour
{
	public PlayerInfoScript PlayerInfo;
	public CampaignScreenScript CampaignScreen;
	public GameObject Character;
	public GameObject ContinueButton;
	public GameObject NextScreen;
	public bool Locked;
	public bool Grow;
	public int ID;
}
