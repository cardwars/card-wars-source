using UnityEngine;

public class CWDeckInventoryController : MonoBehaviour
{
	public CWDeckInventory Table;
	public GameObject labelPanel;
	public UITweener creaturesTweener;
	public UITweener spellsTweener;
	public UITweener buildingsTweener;
	public UITweener heroesTweener;
	public CardType currentTable;
}
