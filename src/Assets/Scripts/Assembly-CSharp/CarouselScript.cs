using UnityEngine;

public class CarouselScript : MonoBehaviour
{
	public float Rotation;
	public float Limit;
	public float Destination;
	public float Threshold;
	public float Speed;
	public float StartPosition;
	public float TouchStart;
	public float TouchCurrent;
	public float TouchDelta;
	public int Target;
	public bool IgnoreClick;
	public bool Lerp;
}
