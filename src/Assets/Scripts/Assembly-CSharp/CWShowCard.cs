using UnityEngine;

public class CWShowCard : MonoBehaviour
{
	public GameObject tweenToObj;
	public CWRevealCard revealScript;
	public GameObject RevealAnimation;
	public GameObject tweenToHide;
	public GameObject FlipVFX;
	public bool DebugVFX;
	public float Delay;
}
