using UnityEngine;

public class CWDeckBuildDeckController : MonoBehaviour
{
	public CWDeckDeckCards Table;
	public CWDeckHeroPanel heroPanel;
	public CWDeckNameplate namePlate;
	public ChooseBattleDeck BattleDeckButton;
	public CardType currentTable;
}
