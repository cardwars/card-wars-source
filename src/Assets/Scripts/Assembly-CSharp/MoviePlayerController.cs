using UnityEngine;

public class MoviePlayerController : MonoBehaviour
{
	public string MovieName;
	public bool CanSkip;
	public bool PlayOnEnable;
	public bool PlayOnClick;
	public int MaxLifetimePlays;
}
