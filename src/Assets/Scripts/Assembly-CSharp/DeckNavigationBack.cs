using UnityEngine;

public class DeckNavigationBack : MonoBehaviour
{
	public GameObject TweenController_Build;
	public GameObject TweenController_Fuse;
	public GameObject TweenController_Inventory;
	public GameObject TweenController_Sell;
	public GameObject TweenController_SellConfirm;
}
