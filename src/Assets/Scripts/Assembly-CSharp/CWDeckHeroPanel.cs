using UnityEngine;

public class CWDeckHeroPanel : MonoBehaviour
{
	public UILabel AbilityLabel;
	public UISprite HeroSprite;
	public UILabel HeroNameLabel;
	public UILabel HPLabel;
	public UILabel LevelLabel;
	public UILabel XPLabel;
	public UILabel DeckSizeLabel;
	public UILabel HeroCardsLabel;
}
