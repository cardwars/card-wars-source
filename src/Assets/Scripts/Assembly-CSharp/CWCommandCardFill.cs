using UnityEngine;

public class CWCommandCardFill : MonoBehaviour
{
	public int playerType;
	public int lane;
	public bool creatureFlag;
	public bool canFloop;
	public bool canAct;
	public GameObject creatureObj;
	public GameObject floopButton;
	public GameObject actionButton;
	public GameObject closeButton;
	public int bonusATK;
	public int bonusDEF;
}
