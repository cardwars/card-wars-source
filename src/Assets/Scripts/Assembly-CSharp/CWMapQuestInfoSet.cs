using UnityEngine;

public class CWMapQuestInfoSet : MonoBehaviour
{
	public UISprite[] stars;
	public UISprite charIcon;
	public UISprite unlockedFrameIcon;
	public UISprite lockedFrameIcon;
}
