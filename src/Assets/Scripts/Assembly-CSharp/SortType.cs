public enum SortType
{
	NONE = 0,
	ATK = 1,
	DEF = 2,
	TYPE = 3,
	NAME = 4,
	FACT = 5,
	RARE = 6,
	MP = 7,
}
