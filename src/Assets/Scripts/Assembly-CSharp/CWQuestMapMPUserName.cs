using Multiplayer;
using UnityEngine;

public class CWQuestMapMPUserName : AsyncData<MultiplayerData>
{
	public UILabel Label;
	public UIButtonTween ShowBottomInfo;
	public GameObject enterMapEvents;
	public GameObject BadNameWarning;
	public UIButtonTween CloseTween;
}
