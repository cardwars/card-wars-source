using UnityEngine;

public class CWDeckOpenPanel : MonoBehaviour
{
	public GameObject CreatureControl;
	public GameObject BuildingControl;
	public GameObject SpellsControl;
	public CardType PanelType;
}
