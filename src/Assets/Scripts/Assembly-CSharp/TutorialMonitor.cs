using UnityEngine;

public class TutorialMonitor : MonoBehaviour
{
	public GameObject landscapeCardTutorialText;
	public GameObject playersFirstTurnLabel;
	public TutorialBattleRing attackTutorialRing;
	public TutorialBattleRing defendTutorialRing;
	public UITweener MainMenuTweener;
	public CWCardTutorial cardTutorial;
}
