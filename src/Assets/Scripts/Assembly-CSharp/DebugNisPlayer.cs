using UnityEngine;
using UnityEngine.UI;

public class DebugNisPlayer : NisSequence
{
	public GameObject nisRoot;
	public GameObject skipPromptGO;
	public Transform nisDialog;
	public Button nisListItemPrefab;
	public string nisResourcePath;
}
