using UnityEngine;

public class DebugDisplayCurrentPhase : MonoBehaviour
{
	public UILabel currentPhaseLabel;
	public GameObject currentPhaseObj;
	public GameObject currentPhaseParent;
	public GameObject obj;
}
