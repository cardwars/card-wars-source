using UnityEngine;

public class DebugGUI : MonoBehaviour
{
	public int typeWidth;
	public int buttonWidth;
	public int defaultHeight;
	public int boxHeight;
	public int boxWidth;
	public int numButtonsWidth;
	public int numButtonsHeight;
	public int smallButtonWidth;
	public int posX;
	public int posY;
}
