public class QuestStats
{
	public int[] UsedTypes;
	public int NumTurns;
	public int[] UsedFactions;
	public int NumCreaturesDefeated;
	public int NumActionPointsPerCard;
	public int NumActionPointsTotal;
	public int NumFloopsUsed;
	public int HPLost;
	public int NumCreaturesLost;
	public int NumLandscapesUsed;
	public int DeckCost;
}
