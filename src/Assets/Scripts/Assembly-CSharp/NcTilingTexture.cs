public class NcTilingTexture : NcEffectBehaviour
{
	public float m_fTilingX;
	public float m_fTilingY;
	public float m_fOffsetX;
	public float m_fOffsetY;
	public bool m_bFixedTileSize;
}
