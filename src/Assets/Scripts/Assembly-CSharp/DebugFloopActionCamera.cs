using UnityEngine;

public class DebugFloopActionCamera : MonoBehaviour
{
	public bool SpawnVolcano;
	public bool SpawnBloodstorm;
	public bool SpawnGeneric;
	public GameObject Volcano;
	public GameObject Bloodstorm;
	public GameObject Generic;
}
