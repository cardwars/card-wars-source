using UnityEngine;

public class MessageController : MonoBehaviour
{
	public MenuController menu;
	public UILabel message;
	public GameObject gemInfo;
	public UILabel gems;
	public GameObject coinInfo;
	public UILabel coin;
}
