using UnityEngine;

public class CWCommandCardSet : MonoBehaviour
{
	public CWCommandCardFill cardFillScript;
	public int playerType;
	public int lane;
	public bool creatureFlag;
	public GameObject creatureObj;
	public bool debugMode;
	public GameObject floopButton;
}
