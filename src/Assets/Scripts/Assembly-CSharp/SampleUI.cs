using UnityEngine;

public class SampleUI : MonoBehaviour
{
	public GUISkin skin;
	public Color titleColor;
	public bool showStatusText;
	public string helpText;
	public bool showHelpButton;
	public bool showHelp;
}
