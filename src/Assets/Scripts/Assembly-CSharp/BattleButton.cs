using UnityEngine;

public class BattleButton : MonoBehaviour
{
	public UILabel magicPointsLabel;
	public UITweener buttonTweener;
	public GameObject onClickTarget;
}
