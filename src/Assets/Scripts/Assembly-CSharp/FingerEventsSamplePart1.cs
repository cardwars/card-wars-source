using UnityEngine;

public class FingerEventsSamplePart1 : SampleBase
{
	public GameObject fingerDownObject;
	public GameObject fingerStationaryObject;
	public GameObject fingerHoverObject;
	public GameObject fingerUpObject;
	public float chargeDelay;
	public float chargeTime;
	public float minSationaryParticleEmissionCount;
	public float maxSationaryParticleEmissionCount;
	public Material highlightMaterial;
}
