using UnityEngine;

public class CWDeckDeckCards : MonoBehaviour
{
	public GameObject DeckCardPrefab;
	public GameObject SortButton;
	public UIDraggablePanel draggablePanel;
	public GameObject AddPanel;
	public GameObject SortPanel;
	public UILabel CreaturesStack;
	public UILabel SpellsStack;
	public UILabel BuildingsStack;
}
