using UnityEngine;
using UnityEngine.UI;

public class FCUpsellController : MonoBehaviour
{
	public UIButtonTween FCUpsellTweenShow;
	public UIButtonTween FCUpsellTweenHide;
	public Text FCPrice;
	public bool AllowPurchase;
	public UIButtonTween SuccessfulPurchase;
}
