public enum CharAnimType
{
	Idle = 0,
	Fidget = 1,
	CardIdle = 2,
	Happy = 3,
	Sad = 4,
	PlayCard = 5,
	PlayRare = 6,
	LastCard = 7,
	PlayRareLastCard = 8,
	DweebDrink = 9,
	IntroP1 = 10,
	IntroP2 = 11,
	Defeated = 12,
}
