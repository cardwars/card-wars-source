using UnityEngine;

public class TriggerVFX : MonoBehaviour
{
	public GameObject VFX;
	public float Scale;
	public Transform Anchor;
	public bool useCustomCoords;
	public Vector3 CustomCoords;
	public bool ChildOfAnchor;
}
