using UnityEngine;

public class YouGotThisController : MonoBehaviour
{
	public GameObject Card;
	public GameObject LeaderCardObj;
	public UIButtonTween RewardShow;
	public AudioClip RewardClip;
	public UILabel RewardText;
	public GameObject IconReward;
	public UILabel GiftRewardText;
}
