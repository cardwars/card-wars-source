using UnityEngine;
using UnityEngine.UI;

public class SideQuestPanel : MonoBehaviour
{
	public Animator anim;
	public Button panelButton;
	public Image NPCSlot;
	public Text Dialog;
	public Image ItemSlot;
}
