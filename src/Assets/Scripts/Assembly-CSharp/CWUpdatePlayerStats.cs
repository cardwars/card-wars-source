using UnityEngine;

public class CWUpdatePlayerStats : MonoBehaviour
{
	public UILabel nameLabel;
	public UILabel staminaLabel;
	public UILabel staminaMaxLabel;
	public UIFilledSprite staminaBar;
	public UILabel staminaTimer;
	public UILabel rankLabel;
	public UILabel coinLabel;
	public UILabel gemLabel;
	public GameObject gemSprite;
	public GameObject heartSprite;
	public UILabel NumTrophies;
	public UILabel inventoryLabel;
	public UIFilledSprite xpBar;
	public UILabel xpLabel;
	public bool holdUpdateFlag;
}
