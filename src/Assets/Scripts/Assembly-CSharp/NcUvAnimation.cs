public class NcUvAnimation : NcEffectAniBehaviour
{
	public float m_fScrollSpeedX;
	public float m_fScrollSpeedY;
	public float m_fTilingX;
	public float m_fTilingY;
	public float m_fOffsetX;
	public float m_fOffsetY;
	public bool m_bUseSmoothDeltaTime;
	public bool m_bFixedTileSize;
	public bool m_bRepeat;
	public bool m_bAutoDestruct;
}
