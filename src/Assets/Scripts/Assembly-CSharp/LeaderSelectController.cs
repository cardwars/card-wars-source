using UnityEngine;

public class LeaderSelectController : MonoBehaviour
{
	public UILabel HeroName;
	public UISprite HeroIcon;
	public GameObject Labels;
}
