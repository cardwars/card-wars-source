using UnityEngine;

public class CWPortraitAbility : MonoBehaviour
{
	public int playerType;
	public GameObject abilityDisplayPanel;
	public GameObject confirmationPanel;
	public UISprite portrait;
	public BoxCollider col;
}
