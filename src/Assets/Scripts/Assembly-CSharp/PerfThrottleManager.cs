using UnityEngine;

public class PerfThrottleManager : MonoBehaviour
{
	public enum PerfEvents
	{
		NONE = 0,
		BATTLE_SEQUENCE = 1,
		BATTLE_LANE1 = 2,
		BATTLE_LANE2 = 3,
		BATTLE_LANE3 = 4,
		BATTLE_LANE4 = 5,
	}

}
