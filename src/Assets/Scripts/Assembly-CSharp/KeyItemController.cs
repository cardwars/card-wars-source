using UnityEngine;

public class KeyItemController : MonoBehaviour
{
	public UILabel Label;
	public UISprite Icon;
	public GameObject Highlight;
}
