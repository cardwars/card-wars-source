using UnityEngine;

public class CWResurrectPlayer : MonoBehaviour
{
	public GameObject noGemTween;
	public GameObject reshuffleTween;
	public CWUpdatePlayerData PlayerData;
	public BattleJukeboxScript jukeBox;
}
