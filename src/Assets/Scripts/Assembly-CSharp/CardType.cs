public enum CardType
{
	Creature = 0,
	Building = 1,
	Spell = 2,
	Dweeb = 3,
	None = 4,
}
