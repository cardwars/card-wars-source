using UnityEngine;

public class CWCreatureStatsFloorDisplay : MonoBehaviour
{
	public int playerType;
	public int lane;
	public UILabel atkLabel;
	public GameObject floopButton;
	public BoxCollider creatureInfoCol;
	public bool displayFlag;
}
