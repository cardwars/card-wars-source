using UnityEngine;
using System.Collections.Generic;

public class CWDeckController : MonoBehaviour
{
	public List<GameObject> Tabs;
	public List<GameObject> Tables;
	public List<GameObject> Bars;
	public CWDeckHeroPanel heroPanel;
	public CWDeckNameplate nameplate;
	public BuildingPos buildingBarPos;
}
