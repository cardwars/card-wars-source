using UnityEngine;

public class NisTriggerSfx : NisComponent
{
	public float playDelaySecs;
	public AudioClip clip;
	public bool autoPlay;
}
