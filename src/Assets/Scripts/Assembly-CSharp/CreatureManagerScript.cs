using UnityEngine;

public class CreatureManagerScript : MonoBehaviour
{
	public GameObject Coin;
	public AudioClip CreatureDeath;
	public GameObject creatureHPBar;
	public bool OpponentOneHitKills;
	public bool PlayerOneHitKills;
}
