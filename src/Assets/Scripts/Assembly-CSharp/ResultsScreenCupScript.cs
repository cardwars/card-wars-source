using UnityEngine;

public class ResultsScreenCupScript : MonoBehaviour
{
	public GameObject DweebCard;
	public Transform ResultsCards;
	public Transform Destination;
	public bool SpawnedCards;
	public float Rotation;
	public float Timer;
	public int CupID;
}
