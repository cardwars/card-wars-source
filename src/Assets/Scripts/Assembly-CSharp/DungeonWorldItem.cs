using UnityEngine;

public class DungeonWorldItem : MonoBehaviour
{
	public GameObject WidgetHighlight;
	public GameObject WidgetLocked;
	public UILabel LabelName;
	public UILabel LabelUnlockTime;
	public UISprite Icon;
}
