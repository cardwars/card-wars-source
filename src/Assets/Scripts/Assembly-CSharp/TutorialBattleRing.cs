using UnityEngine;

public class TutorialBattleRing : MonoBehaviour
{
	public GameObject bg;
	public GameObject[] bars;
	public GameObject[] labels;
	public UIButtonTween showTween;
	public UIButtonTween hideTween;
}
