using UnityEngine;

public class NcAddForce : NcEffectBehaviour
{
	public Vector3 m_AddForce;
	public Vector3 m_RandomRange;
	public ForceMode m_ForceMode;
}
