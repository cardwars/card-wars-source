using UnityEngine;

public class CWQuestMapAdditiveLoad : AsyncLoader
{
	public string questMapScene;
	public string fcQuestMapScene;
	public GameObject mapBoardButton;
	public GameObject playerStats;
	public Transform mapCamPosition;
	public Transform mapCamTargetPosition;
}
