using UnityEngine;

public class SummonScript : MonoBehaviour
{
	public GameObject SpawnEffect;
	public AudioClip SpawnAudio;
	public float SpawnAudioDelaySecs;
	public string Intro;
	public string Idle;
	public float IntroLength;
	public GameObject hpBar;
}
