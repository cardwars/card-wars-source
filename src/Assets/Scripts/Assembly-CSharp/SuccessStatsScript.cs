using UnityEngine;

public class SuccessStatsScript : MonoBehaviour
{
	public UILabel LblQuestID;
	public UILabel LblCardsVal;
	public UILabel LblCoinsVal;
	public UILabel LblXPVal;
	public UILabel LblXPBar;
	public UILabel LblConditionTxt;
	public UILabel LblConditionStatus;
	public UISprite SprConditionStatus;
	public UIFilledSprite SprXPBar;
	public float DelayInterval;
	public GameObject[] TweenControllers;
	public float XPBarInterval;
	public GameObject LvlUpTween;
}
