using UnityEngine;

public class ResultsScreenButtonScript : MonoBehaviour
{
	public GameObject WinnerEarnings;
	public GameObject LoserEarnings;
	public GameObject RewardCard;
	public ResultsCardsScript ResultsCards;
	public UILabel CoinsLabel;
	public UILabel HeaderLabel;
	public UILabel RewardLabel;
	public UILabel LoserRewardLabel;
	public int Phase;
}
