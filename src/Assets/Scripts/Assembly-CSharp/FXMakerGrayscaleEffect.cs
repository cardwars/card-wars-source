using UnityEngine;

public class FXMakerGrayscaleEffect : FXMakerImageEffectBase
{
	public Texture textureRamp;
	public float rampOffset;
}
