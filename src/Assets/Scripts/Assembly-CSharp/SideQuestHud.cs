using UnityEngine;
using UnityEngine.UI;

public class SideQuestHud : MonoBehaviour
{
	public Image ItemSlot;
	public Text ItemCount;
	public Animator anim;
}
