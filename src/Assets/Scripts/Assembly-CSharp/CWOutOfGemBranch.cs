using UnityEngine;

public class CWOutOfGemBranch : MonoBehaviour
{
	public GameObject okTween;
	public GameObject errorTween;
	public AudioClip okSound;
	public AudioClip errorSound;
}
