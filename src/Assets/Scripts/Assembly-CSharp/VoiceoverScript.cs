using UnityEngine;

public class VoiceoverScript : MonoBehaviour
{
	public AudioClip ImmortalMaiseWalker;
	public AudioClip P1_SetupPhase_1;
	public AudioClip P1_SetupPhase_2;
	public AudioClip P1_SetupPhase_3;
	public AudioClip P1_BattlePhase_1;
	public AudioClip P2_SetupPhase_1;
	public AudioClip P2_BattlePhase_1;
	public AudioClip P2_BattlePhase_2;
	public AudioClip P1_Wins;
	public AudioClip P2_Wins;
	public Renderer Mouth;
	public Texture Mouth_MPandB;
	public Texture Mouth_AltTH;
	public Texture Mouth_OpenSmall;
	public Texture Mouth_AandI;
	public Texture Mouth_O;
	public Texture Mouth_Consonants;
	public Texture Mouth_LandTH;
	public Texture Mouth_AltConsonants;
	public Texture Mouth_WandQ;
	public Texture Mouth_OpenSmall2;
	public bool JakePlaysMaiseWalker;
	public bool AudioPlayed;
	public float Timer;
}
