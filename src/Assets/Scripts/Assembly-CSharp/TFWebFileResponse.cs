using System.Net;

public class TFWebFileResponse
{
	public HttpStatusCode StatusCode;
	public string Data;
	public string URI;
	public bool NetworkDown;
}
