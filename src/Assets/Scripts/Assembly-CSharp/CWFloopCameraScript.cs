using UnityEngine;

public class CWFloopCameraScript : MonoBehaviour
{
	public float Delay;
	public Transform CameraLocation;
	public Transform CameraTarget;
	public Camera FloopCamera;
}
