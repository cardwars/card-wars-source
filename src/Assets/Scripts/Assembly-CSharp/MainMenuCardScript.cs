using UnityEngine;

public class MainMenuCardScript : MonoBehaviour
{
	public GameObject NextScreen;
	public CarouselScript CarouselScript;
	public GameObject FBX;
	public GameObject Jukebox;
	public AudioClip NewMusic;
	public bool DoNotProceed;
	public int ID;
}
