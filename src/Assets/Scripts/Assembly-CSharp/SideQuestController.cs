using UnityEngine;

public class SideQuestController : MonoBehaviour
{
	public string SideQuestGroup;
	public GameObject HudCanvas;
	public GameObject PanelCanvas;
	public string SQPanelPrefabPath;
	public string SQHudPrefabPath;
}
