using UnityEngine;

public class CWShufflingPopup : MonoBehaviour
{
	public AudioClip cardShufflePanelSound;
	public AudioClip cardShuffleSound;
	public int playerInteger;
	public CWUpdatePlayerData PlayerData;
}
