using UnityEngine;

public class TweenPositionAnchored : TweenPosition
{
	public bool tweenAnchorScreenOffset;
	public string onTweenEnableForwardMessage;
	public GameObject onTweenEnableForwardMessageTarget;
	public string onTweenEnableReverseMessage;
	public GameObject onTweenEnableReverseMessageTarget;
}
