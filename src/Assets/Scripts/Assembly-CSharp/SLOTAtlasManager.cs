public class SLOTAtlasManager : SLOTGameSingleton<SLOTAtlasManager>
{
	public UIAtlas[] atlases;
	public UIAtlas[] lowresAtlases;
	public UIFont[] fonts;
	public UIFont[] lowresFonts;
}
