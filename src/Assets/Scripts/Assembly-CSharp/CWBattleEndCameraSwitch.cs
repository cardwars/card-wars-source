using UnityEngine;

public class CWBattleEndCameraSwitch : MonoBehaviour
{
	public Camera[] disableCameras;
	public Camera expandCamera;
	public Camera shrinkCamera;
	public bool shrinkP2;
}
