using UnityEngine;

public class FxmTestSingleMain : MonoBehaviour
{
	public GameObject[] m_EffectPrefabs;
	public GUIText m_EffectGUIText;
	public int m_nIndex;
	public float m_fCreateScale;
	public int m_nCreateCount;
	public float m_fRandomRange;
}
