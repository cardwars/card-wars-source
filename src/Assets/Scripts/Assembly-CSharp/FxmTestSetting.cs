using UnityEngine;

public class FxmTestSetting : MonoBehaviour
{
	public int m_nPlayIndex;
	public int m_nTransIndex;
	public FxmTestControls.AXIS m_nTransAxis;
	public float m_fTransRate;
	public float m_fStartPosition;
	public float m_fDistPerTime;
	public int m_nRotateIndex;
	public int m_nMultiShotCount;
}
