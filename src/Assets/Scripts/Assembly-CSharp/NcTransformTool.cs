using UnityEngine;

public class NcTransformTool
{
	public Vector3 m_vecPos;
	public Quaternion m_vecRot;
	public Vector3 m_vecRotHint;
	public Vector3 m_vecScale;
}
