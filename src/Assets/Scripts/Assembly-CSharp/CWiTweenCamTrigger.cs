using UnityEngine;

public class CWiTweenCamTrigger : MonoBehaviour
{
	public GameObject gameCamera;
	public GameObject gameCameraTarget;
	public string tweenName;
	public bool followFlag;
}
