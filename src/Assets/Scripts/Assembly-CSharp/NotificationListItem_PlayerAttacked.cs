public class NotificationListItem_PlayerAttacked : NotificationListItem
{
	public UILabel titleLabel;
	public UILabel currentRankLabel;
	public UILabel currentRankValueLabel;
	public UILabel previousRankLabel;
	public UILabel previousRankValueLabel;
	public UILabel winsLabel;
	public UILabel winsValueLabel;
	public UILabel lossesLabel;
	public UILabel lossesValueLabel;
	public UISprite trophyIcon;
	public UILabel trophiesPlusMinusLabel;
	public UILabel trophiesLabel;
	public UILabel bestStreakLabel;
	public UILabel bestStreakValueLabel;
}
