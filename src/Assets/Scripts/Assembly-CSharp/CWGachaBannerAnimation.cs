using UnityEngine;

public class CWGachaBannerAnimation : MonoBehaviour
{
	public Animation BannerRarity1;
	public Animation BannerRarity2;
	public Animation BannerRarity3;
	public Animation BannerRarity4;
	public Animation BannerRarity5;
	public Transform NormalRarity;
	public Transform PremiumRarity;
	public Transform HideRarity;
	public float NormalDelay;
	public float PremiumDelay;
	public bool debugRarityFlag;
	public int debugRarity;
	public UIButtonPlayAnimation AnimationTrigger;
}
