using UnityEngine;

public class CWGachaBannerSpawn : MonoBehaviour
{
	public GameObject[] bannerObjects;
	public Transform parentTr;
	public float PremiumDelay;
	public float NormalDelay;
	public float delay;
	public float bannerScale;
	public bool debugRarityFlag;
	public int debugRarity;
}
