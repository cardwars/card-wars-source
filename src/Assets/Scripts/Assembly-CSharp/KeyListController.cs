using UnityEngine;

public class KeyListController : MonoBehaviour
{
	public UIButtonTween ShowTween;
	public UIButtonTween HideTween;
	public GameObject TemplateItem;
	public UIGrid Grid;
	public UISprite ItemIcon;
	public UILabel ItemType;
	public UILabel ItemInfo;
	public GameObject BackCollider;
}
