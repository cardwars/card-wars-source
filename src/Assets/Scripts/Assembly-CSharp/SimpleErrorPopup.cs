using UnityEngine;

public class SimpleErrorPopup : MonoBehaviour
{
	public UILabel titleLabel;
	public UILabel messageLabel;
	public GameObject[] buttons;
	public bool destroyWhenDone;
}
