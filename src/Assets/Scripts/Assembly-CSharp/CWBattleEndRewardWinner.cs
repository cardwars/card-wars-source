using UnityEngine;

public class CWBattleEndRewardWinner : AsyncData<string>
{
	public UILabel TrophyEarned;
	public UILabel StreakPanelWinStreak;
	public UILabel StreakPanelWinBonus;
	public UILabel StreakBonus;
	public UILabel TotalTrophyEarned;
	public GameObject[] StarConditions;
	public GameObject StreakPanel;
}
