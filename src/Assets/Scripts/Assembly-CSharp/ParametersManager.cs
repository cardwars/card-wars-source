public class ParametersManager
{
	public int Min_Cards_In_Inventory;
	public int Min_Creatures_In_Inventory;
	public int Max_Duplicates_In_Deck;
	public int Min_Cards_In_Deck;
	public int New_Player_Max_Inventory;
	public int New_Player_Max_Stamina;
	public int New_Player_Coins;
	public int New_Player_Gems;
	public int Stamina_Restoration_Rate;
	public int Max_Leader_Level;
	public int Starting_Magic_Points;
	public int Max_Magic_Points;
	public int Restart_OnResume_Time;
	public int Min_Dungeon_Level;
	public string Calendar_Unlock_Tutorial;
	public string GatchaKey_Unlock_Tutorial;
	public string AIDeck_Bad_Card_Swap;
	public int BonusQuest_firstquest_FC;
	public int BonusQuest_timelapse_FC;
	public int BonusQuest_matchlapse_FC;
	public string FC_CardReward_FirstTime;
	public int SideQuest_firstquest_FC;
	public int SideQuest_matchlapse_FC;
	public int SideQuest_firstquest_Main;
	public int SideQuest_matchlapse_Main;
}
