using UnityEngine;

public class CodeRedemptionScript : MonoBehaviour
{
	public UILabel Label;
	public AudioClip fanfare;
	public AudioClip success;
	public AudioClip failure;
	public AudioClip pig;
	public AudioClip cerebral;
	public UIButtonTween ShowSuccess;
	public UIButtonTween Showfail;
	public UIButtonTween ShowDupe;
	public UILabel CreatureName;
}
