using UnityEngine;

public class FlippedLandscapeScript : MonoBehaviour
{
	public Collider MyCollider;
	public bool Flash;
	public float Alpha;
	public AudioClip SelectionSound;
	public bool doNotStopHighlightFlag;
}
