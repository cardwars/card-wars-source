using UnityEngine;

public class SLOTFacebookFriendInfo
{
	public string facebookID;
	public string name;
	public string pictureURL;
	public Texture pictureTexture;
	public bool isRegistered;
	public int userID;
	public int gameID;
}
