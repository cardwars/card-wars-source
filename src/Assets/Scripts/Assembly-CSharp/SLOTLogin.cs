using UnityEngine;

public class SLOTLogin : MonoBehaviour
{
	public NextScreenScript nextScript;
	public GameObject name_label;
	public GameObject continue_button;
	public GameObject back_button;
	public GameObject status_text;
	public GameObject newPlayerDestination;
	public GameObject oldPlayerDestination;
	public UILabel StatusLabel;
}
