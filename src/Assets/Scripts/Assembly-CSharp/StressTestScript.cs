using UnityEngine;

public class StressTestScript : MonoBehaviour
{
	public GameObject MainCamera;
	public GameObject MainCameraChildren;
	public Transform StressTestCameraPivot;
	public int SpinSpeed;
}
