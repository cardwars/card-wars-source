public enum VOEvent
{
	Opening = 0,
	Win = 1,
	Lose = 2,
	DestroyCreature = 3,
	PlayCreature = 4,
	PlayBuilding = 5,
	PlaySpell = 6,
	CreatureDestroyed = 7,
	BuildingDestroyed = 8,
	Floop = 9,
	HeroDamaged = 10,
	LeaderAbility = 11,
}
