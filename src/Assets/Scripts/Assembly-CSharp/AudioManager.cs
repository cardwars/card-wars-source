using UnityEngine;

public class AudioManager : MonoBehaviour
{
	public AudioClip PlaceCard1;
	public AudioClip PlaceCard2;
	public AudioClip PlaceCard3;
	public AudioClip PlaceCard4;
	public AudioClip PlaceCard5;
	public AudioClip DrawCard;
}
