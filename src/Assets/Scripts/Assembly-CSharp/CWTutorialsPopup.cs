using UnityEngine;

public class CWTutorialsPopup : MonoBehaviour
{
	public string tutorial_ID;
	public AudioClip VOClip;
	public float VOLength;
	public bool onlyOnce;
}
