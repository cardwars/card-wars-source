using UnityEngine;

public class CWiTweenTrigger : MonoBehaviour
{
	public GameObject[] tweenTargets;
	public string tweenName;
	public string tweenNameOnPressTrue;
	public string tweenNameOnPressFalse;
}
