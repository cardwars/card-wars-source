public class NcDetachParent : NcEffectBehaviour
{
	public bool m_bFollowParentTransform;
	public bool m_bParentHideToStartDestroy;
	public float m_fSmoothDestroyTime;
	public bool m_bDisableEmit;
	public bool m_bSmoothHide;
	public bool m_bMeshFilterOnlySmoothHide;
}
