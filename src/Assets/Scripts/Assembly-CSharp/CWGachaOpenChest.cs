using UnityEngine;

public class CWGachaOpenChest : MonoBehaviour
{
	public CWMenuCameraTarget cameraTarget;
	public GameObject NormalCard;
	public GameObject PremiumCard;
	public GameObject VFX_RarityLow;
	public GameObject VFX_RarityMed;
	public GameObject VFX_RarityHigh;
	public GameObject VFX_Premium_Open;
	public GameObject VFX_Normal_Open;
}
