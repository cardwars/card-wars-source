using UnityEngine;

public class CWBattleEndPlayerStats : MonoBehaviour
{
	public UILabel nameLabel;
	public UILabel staminaLabel;
	public UIFilledSprite staminaBar;
	public UILabel staminaTimer;
	public UILabel rankLabel;
	public UILabel coinLabel;
	public UILabel gemLabel;
	public UILabel inventoryLabel;
	public UILabel inventoryMaxLabel;
	public UIFilledSprite xpBar;
	public UILabel xpLabel;
	public UILabel xpToNextLabel;
	public int ssCoin;
	public int ssGem;
	public int ssNumStars;
	public UILabel NumTrophies;
	public string ssSchemeID;
	public int ssRank;
	public int ssXP;
	public int ssHP;
}
