using UnityEngine;

public class PVPInfoHUD : MonoBehaviour
{
	public GameObject ParentObject;
	public UILabel PVPPlayerName;
	public UILabel PVPOpponentName;
	public UILabel PVPTrophyWin;
	public UILabel PVPTrophyLoss;
}
