using UnityEngine;

public class NcParticleSpiral : NcEffectBehaviour
{
	public float m_fDelayTime;
	public GameObject m_ParticlePrefab;
	public int m_nNumberOfArms;
	public int m_nParticlesPerArm;
	public float m_fParticleSeparation;
	public float m_fTurnDistance;
	public float m_fVerticalTurnDistance;
	public float m_fOriginOffset;
	public float m_fTurnSpeed;
	public float m_fFadeValue;
	public float m_fSizeValue;
	public int m_nNumberOfSpawns;
	public float m_fSpawnRate;
}
