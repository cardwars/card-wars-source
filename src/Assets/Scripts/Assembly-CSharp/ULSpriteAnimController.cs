using UnityEngine;

public class ULSpriteAnimController
{
	public bool animate;
	public int[] uvToVertMap;
	public ULSpriteAnimationSetting currentAnimationSetting;
	public MeshFilter quad;
}
