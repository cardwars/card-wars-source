using UnityEngine;

public class CWLandscapeCardDragOld : MonoBehaviour
{
	public int Index;
	public Camera uiCamera;
	public AudioClip SummonSound;
	public AudioClip CardMove;
	public AudioClip[] PlaceCardSounds;
	public bool FollowFinger;
	public bool Return;
	public string currentTypeName;
}
