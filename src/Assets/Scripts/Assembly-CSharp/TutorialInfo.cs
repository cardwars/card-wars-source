using UnityEngine;

public class TutorialInfo
{
	public string TutorialID;
	public string Flow;
	public string TweenTrigger;
	public bool CanOverride;
	public bool UseInputEnabler;
	public bool UseInputEnablerWhenDone;
	public string PointerTargetMax;
	public string Title;
	public Vector3 Pos;
	public Vector3 Rot;
	public Vector3 Scale;
	public string Layer;
	public string Text;
	public int AddKeys;
	public Vector3 TextPos;
	public int TextWidth;
	public string Sprite;
	public Vector3 SpritePos;
	public Vector3 SpriteSize;
	public Vector3 SpriteRot;
	public bool IsFinal;
	public bool IsLastInFlow;
	public TutorialTrigger Trigger;
	public TutorialTrigger DependencyTrigger;
	public bool Reusable;
	public bool PauseGame;
	public string VOClip;
	public bool UseVOScale;
	public bool Skippable;
	public bool OnPress;
	public bool DimBackground;
	public bool DimFadeIn;
	public bool DimFadeOut;
	public bool DimDestroyOnClose;
	public bool dummy;
}
