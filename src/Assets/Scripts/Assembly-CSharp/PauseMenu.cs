using UnityEngine;

public class PauseMenu : MonoBehaviour
{
	public string pausedMessage;
	public string cantPauseDuringTutorialMessage;
	public UILabel messageLabel;
	public GameObject yesButton;
	public GameObject noButton;
	public GameObject okButton;
}
