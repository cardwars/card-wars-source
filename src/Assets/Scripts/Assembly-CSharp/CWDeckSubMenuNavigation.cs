using UnityEngine;

public class CWDeckSubMenuNavigation : MonoBehaviour
{
	public ScreenDimmer questMapDimmer;
	public bool NavToBuildDeck;
	public bool NavToSell;
	public bool NavToInventory;
	public bool NavToCrafting;
}
