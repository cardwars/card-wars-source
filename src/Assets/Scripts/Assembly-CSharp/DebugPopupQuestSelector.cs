using UnityEngine;
using UnityEngine.UI;

public class DebugPopupQuestSelector : MonoBehaviour
{
	public GameObject questButtonPrefab;
	public CanvasRenderer panelLevels;
	public Button buttonClose;
}
