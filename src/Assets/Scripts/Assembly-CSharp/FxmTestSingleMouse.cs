using UnityEngine;

public class FxmTestSingleMouse : MonoBehaviour
{
	public Transform m_TargetTrans;
	public Camera m_GrayscaleCamara;
	public Shader m_GrayscaleShader;
	public float m_fDistance;
	public float m_fXSpeed;
	public float m_fYSpeed;
	public float m_fWheelSpeed;
	public float m_fYMinLimit;
	public float m_fYMaxLimit;
	public float m_fDistanceMin;
	public float m_fDistanceMax;
	public int m_nMoveInputIndex;
	public int m_nRotInputIndex;
	public float m_fXRot;
	public float m_fYRot;
}
