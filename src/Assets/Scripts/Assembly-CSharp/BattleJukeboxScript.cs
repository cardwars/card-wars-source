using UnityEngine;

public class BattleJukeboxScript : MonoBehaviour
{
	public AudioClip DefaultTheme;
	public AudioClip Theme1;
	public AudioClip Theme2;
	public AudioClip Theme3;
	public AudioClip Theme4;
	public AudioClip Theme5;
	public AudioClip Theme6;
	public AudioClip Theme7;
	public AudioClip Theme8;
	public AudioClip Theme9;
	public AudioClip Theme10;
	public AudioClip Theme11;
	public AudioClip BattleIntroTheme;
	public AudioClip BattleEndTheme;
}
