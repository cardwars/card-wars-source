using UnityEngine;

public class GlobalFlags
{
	public bool ReturnToMainMenu;
	public bool ReturnToBuildDeck;
	public bool ReturningFromGame;
	public bool NewlyCleared;
	public bool Cleared;
	public bool SkipAnims;
	public int lastQuestConditionStatus;
	public int lastStaminaMax;
	public int lastStamina;
	public int lastGem;
	public Vector3 lastQuestMapCameraIdealPos;
	public float lastQuestMapCameraFOV;
	public bool stopTutorial;
	public bool disableDungeonTimeLock;
	public bool InMPMode;
	public bool enableMapDrag;
}
