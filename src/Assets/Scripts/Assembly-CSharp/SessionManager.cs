using UnityEngine;

public class SessionManager : MonoBehaviour
{
	public GameObject BusyIcon;
	public string PlayerID;
	public string LoginID;
	public string NetState;
	public string DeviceID;
	public bool LocalRemoteSaveGameConflict;
}
