using UnityEngine;

public class DebugDisplayCurrentCard : MonoBehaviour
{
	public GameObject currentCardObj;
	public UILabel currentCardLabel;
	public UILabel currentDecisionLabel;
	public GameObject currentCardParent;
}
