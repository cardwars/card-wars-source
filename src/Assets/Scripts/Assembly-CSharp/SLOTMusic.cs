using UnityEngine;

public class SLOTMusic : MonoBehaviour
{
	public AudioSource[] musicAudioSources;
	public int playOnStartIndex;
	public float volume;
}
