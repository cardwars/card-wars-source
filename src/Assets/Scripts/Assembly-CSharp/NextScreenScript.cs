using UnityEngine;

public class NextScreenScript : MonoBehaviour
{
	public GameObject Jukebox;
	public AudioClip NewMusic;
	public GameObject NextScreen;
	public GameObject NextScreen_Facebook;
	public bool DoNotDestroy;
	public bool ChangeMusic;
	public bool LeaveAllowed;
	public bool LeaveScreen;
	public float GrowthSpeed;
}
