using UnityEngine;

public class CWSpawnObject : MonoBehaviour
{
	public string resourceName;
	public GameObject prefab;
	public Transform parentTr;
	public float delay;
	public bool onEnable;
}
