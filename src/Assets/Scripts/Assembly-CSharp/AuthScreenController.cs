using UnityEngine;

public class AuthScreenController : MonoBehaviour
{
	public ConfirmPopupController GeneralPopup;
	public UIButtonTween GeneralPopupShowTween;
	public UIButtonTween AgeGateShowTween;
	public string NextSceneName;
	public BusyIconController busyIconController;
}
