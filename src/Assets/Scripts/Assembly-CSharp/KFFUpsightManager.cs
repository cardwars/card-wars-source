using UnityEngine;

public class KFFUpsightManager : MonoBehaviour
{
	public string androidAppToken;
	public string androidAppSecret;
	public string gcmProjectNumber;
	public string androidAppTokenDebug;
	public string androidAppSecretDebug;
	public string gcmProjectNumberDebug;
	public string iosAppToken;
	public string iosAppSecret;
	public string iosAppTokenDebug;
	public string iosAppSecretDebug;
}
