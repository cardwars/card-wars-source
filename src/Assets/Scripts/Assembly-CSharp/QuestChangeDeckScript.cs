using UnityEngine;

public class QuestChangeDeckScript : MonoBehaviour
{
	public bool Increment;
	public LandscapePreviewScript Lane1;
	public LandscapePreviewScript Lane2;
	public LandscapePreviewScript Lane3;
	public LandscapePreviewScript Lane4;
	public CWQuestLandscapes LandscapePreviews;
	public GameObject Labels;
	public LeaderSelectController LeaderSelect;
}
