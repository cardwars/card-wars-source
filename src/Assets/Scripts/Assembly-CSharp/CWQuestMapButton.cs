using UnityEngine;

public class CWQuestMapButton : MonoBehaviour
{
	public string daggerAnimation;
	public string idleAnimation;
	public Animation battleMapAnimation;
	public GameObject enterMapEvents;
	public Camera mainMenuCamera;
	public MultiAnimationScript AnimationScripts;
}
