using UnityEngine;
using System.Collections.Generic;

public class PoolManager : MonoBehaviour
{
	public List<GameObject> objectMasterList;
	public List<PreloadEntry> preload;
	public int stackCount;
}
