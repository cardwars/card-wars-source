using UnityEngine;

public class CWCreatureHPBarFaceCam : MonoBehaviour
{
	public int player;
	public int lane;
	public UISprite hpBarSprite;
	public Transform hpBar;
	public GameObject target;
}
