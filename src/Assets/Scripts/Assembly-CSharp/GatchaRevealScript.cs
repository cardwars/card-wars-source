using UnityEngine;

public class GatchaRevealScript : MonoBehaviour
{
	public bool Reveal;
	public GameObject CardObj;
	public float Rotation;
	public bool Premium;
	public TweenTransform prizeTween;
	public GameObject Flair1;
	public GameObject Flair2;
	public GameObject Flair3;
	public GameObject Flair4;
	public GameObject Flair5;
	public GameObject FlairTweenController;
	public Transform DisplayFlair;
	public Transform HiddenFlair;
	public GameObject LeaderCardObj;
	public bool tweenPlayed;
}
