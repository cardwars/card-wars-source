using UnityEngine;

public class DebugDisplayAnimValue : MonoBehaviour
{
	public int player;
	public UILabel animName;
	public UILabel currentTime;
	public UILabel animLength;
	public UILabel faceAnimName;
	public UILabel faceCurrentTime;
	public UILabel characterName;
	public Animation anim;
}
