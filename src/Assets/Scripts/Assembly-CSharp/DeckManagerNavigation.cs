using UnityEngine;

public class DeckManagerNavigation : MonoBehaviour
{
	public GameObject DeckManager;
	public GameObject BuildDeck;
	public GameObject FuseDeck;
	public GameObject Inventory;
	public GameObject SellCard;
	public GameObject BuildBack;
	public GameObject FuseBack;
	public GameObject InventoryBack;
	public GameObject SellBack;
	public GameObject SellConfirmBack;
}
