using UnityEngine;

public class OpenUrlScript : MonoBehaviour
{
	public bool EULA;
	public bool CartoonNetwork;
	public bool Privacy;
	public bool Custom;
	public string LinkCustom;
	public UIButtonTween FailedConnection;
}
