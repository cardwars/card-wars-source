using UnityEngine;

public class CWCameraCollider : MonoBehaviour
{
	public Camera mainCamera;
	public int longColW;
	public int zoomColW;
	public int longColH;
	public int zoomColH;
	public float longFOV;
	public float zoomFOV;
}
