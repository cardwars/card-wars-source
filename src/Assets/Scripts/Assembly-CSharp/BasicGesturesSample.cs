using UnityEngine;

public class BasicGesturesSample : SampleBase
{
	public GameObject longPressObject;
	public GameObject tapObject;
	public GameObject doubleTapObject;
	public GameObject swipeObject;
	public GameObject dragObject;
}
