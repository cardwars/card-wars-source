using UnityEngine;

public class CWSellCardsController : MonoBehaviour
{
	public CWDeckSellCards Table;
	public GameObject labelPanel;
	public UITweener creaturesTweener;
	public UITweener spellsTweener;
	public UITweener buildingsTweener;
	public CardType currentTable;
}
