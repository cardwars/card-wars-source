using UnityEngine;

public class NcDrawFpsRect : MonoBehaviour
{
	public bool centerTop;
	public Rect startRect;
	public bool updateColor;
	public bool allowDrag;
	public float frequency;
	public int nbDecimal;
}
