using UnityEngine;

public class MatchScript : MonoBehaviour
{
	public PlayerInfoScript PlayerInfo;
	public PreMatchScreenScript PreMatchScreen;
	public GameObject ContinueButton;
	public GameObject Background;
	public GameObject NextScreen;
	public bool Grow;
	public int ID;
}
