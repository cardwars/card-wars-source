using UnityEngine;

public class SLOTAudioManager : SLOTGameSingleton<SLOTAudioManager>
{
	public AudioSource gui_audiosource;
	public float soundVolume;
	public float voVolume;
	public float musicVolume;
}
