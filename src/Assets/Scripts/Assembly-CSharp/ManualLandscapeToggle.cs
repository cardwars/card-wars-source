using UnityEngine;

public class ManualLandscapeToggle : MonoBehaviour
{
	public UILabel LandscapeName;
	public UISprite LandscapeArt;
	public UISprite LandscapeFrame;
	public int LaneID;
}
