using UnityEngine;

public class PointCloudGestureSample : SampleBase
{
	public PointCloudGestureRenderer GestureRendererPrefab;
	public float GestureScale;
	public Vector2 GestureSpacing;
	public int MaxGesturesPerRaw;
}
