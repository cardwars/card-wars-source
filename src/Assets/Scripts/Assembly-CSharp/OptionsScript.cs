using UnityEngine;

public class OptionsScript : MonoBehaviour
{
	public UISlider sfxSlider;
	public UISlider voSlider;
	public UISlider musicSlider;
	public ButtonToggleScript toggleBattleWheel;
	public ButtonToggleScript toggleNotifications;
	public ButtonToggleScript toggleLowResolution;
	public GameObject LowResOverride;
	public GameObject AgeReset;
}
