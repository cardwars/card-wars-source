using UnityEngine;

public class LandscapePreviewScript : MonoBehaviour
{
	public int Lane;
	public UISprite icon;
	public UILabel label;
	public UISprite frame;
	public bool IsOpponent;
}
