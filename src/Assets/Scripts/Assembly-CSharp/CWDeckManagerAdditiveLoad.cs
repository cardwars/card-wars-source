using UnityEngine;

public class CWDeckManagerAdditiveLoad : AsyncLoader
{
	public string deckManagerScene;
	public GameObject backToMenuPanel;
	public Transform menuCamPosition;
	public Transform menuCamTargetPosition;
}
