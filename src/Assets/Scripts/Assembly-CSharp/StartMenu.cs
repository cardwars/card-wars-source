using UnityEngine;

public class StartMenu : MonoBehaviour
{
	public GUIStyle titleStyle;
	public GUIStyle buttonStyle;
	public float buttonHeight;
	public Transform itemsTree;
	public float menuWidth;
	public float sideBorder;
}
