using UnityEngine;

public class SocialButtonScript : MonoBehaviour
{
	public UILabel buttonLabel;
	public GameObject buttonObj;
	public ConfirmPopupController confirmPopup;
	public UIButtonTween confirmPopupShow;
}
