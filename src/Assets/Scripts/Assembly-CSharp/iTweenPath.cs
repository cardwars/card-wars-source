using UnityEngine;
using System.Collections.Generic;

public class iTweenPath : MonoBehaviour
{
	public string pathName;
	public Color pathColor;
	public List<Vector3> nodes;
	public int nodeCount;
	public bool initialized;
	public string initialName;
}
