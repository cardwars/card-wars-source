using UnityEngine;

public class NcDuplicator : NcEffectBehaviour
{
	public float m_fDuplicateTime;
	public int m_nDuplicateCount;
	public float m_fDuplicateLifeTime;
	public Vector3 m_AddStartPos;
	public Vector3 m_AccumStartRot;
	public Vector3 m_RandomRange;
}
