using UnityEngine;

public class RefreshMatch : MonoBehaviour
{
	public UILabel Timer;
	public UILabel YourName;
	public UILabel OpponentName;
	public UISprite OpponentPortrait;
	public UILabel LeaderLvl;
	public UILabel LeaderHP;
	public UILabel LeaderDesc;
	public UILabel TrophyWinVal;
	public UILabel TrophyLossVal;
	public GameObject SearchOpponent;
	public CWQuestLandscapes QuestLandscapes;
	public Animation IntroAnim;
	public GameObject PlayButton;
	public int Countdown;
}
