using UnityEngine;

public class AssetLoaderUI : MonoBehaviour
{
	public string startupSceneName;
	public UITexture barTexture;
	public UITexture barBG;
	public GameObject retryButton;
	public UILabel messageLabel;
	public BusyIconController busyIconController;
}
