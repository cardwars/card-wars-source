using UnityEngine;

public class CWResultFillTable : MonoBehaviour
{
	public GameObject LeaderPrefab;
	public GameObject CardPrefab;
	public GameObject RevealTween;
	public CWRevealCard RevealScript;
	public GameObject RevealAnimation;
	public GameObject HideTween;
	public float Interval;
	public CWResultBannerAnimation BannerAnim;
	public GameObject FlipVFX;
}
