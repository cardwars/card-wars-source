using UnityEngine;

public class CWOpponentActionSequencer : MonoBehaviour
{
	public GameObject reshuffleTween;
	public UIButtonTween floopPanelTween;
	public UIButtonTween spellPanelTween;
	public bool resumeFlag;
}
