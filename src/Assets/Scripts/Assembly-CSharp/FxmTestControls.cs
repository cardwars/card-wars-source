using UnityEngine;

public class FxmTestControls : MonoBehaviour
{
	public enum AXIS
	{
		X = 0,
		Y = 1,
		Z = 2,
	}

	public bool m_bMinimize;
	public float m_fTimeScale;
}
