using UnityEngine;

public class PanelManagerDeck : MonoBehaviour
{
	public Camera uiCamera;
	public GameObject activeCard;
	public GameObject blackPanel;
	public GameObject zoomCard;
	public GameObject zoomLeaderCard;
	public GameObject defaultPage;
	public GameObject backToMenuTweenTarget;
}
