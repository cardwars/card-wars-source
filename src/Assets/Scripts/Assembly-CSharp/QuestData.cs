using System.Collections.Generic;

public class QuestData
{
	public string OpponentDeckID;
	public string[] Condition;
	public List<string> CardDrops;
	public string LevelPrefab;
	public string LevelName;
	public string TablePrefab;
	public string ChairPrefab;
	public string UnlockRegion;
	public string ResultLootChestPrefab;
	public string NodePrefabPath;
	public string LoadingScreenTextureName;
}
