public class NcDelayActive : NcEffectBehaviour
{
	public string NotAvailable;
	public float m_fDelayTime;
	public bool m_bActiveRecursively;
	public float m_fParentDelayTime;
}
