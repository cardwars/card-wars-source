using UnityEngine;
using System.Collections.Generic;

public class AnimFadeOut : MonoBehaviour
{
	public Animation anim;
	public List<GameObject> MatsToFadeOut;
	public float startFrame;
	public float endFrame;
	public string animName;
}
