using UnityEngine;

public class KFFKochavaAnalytics : MonoBehaviour
{
	public string kochavaDevAppId;
	public string kochavaDevAppIdIOS;
	public string kochavaDevAppIdAndroid;
	public string kochavaDevAppIdKindle;
}
