using UnityEngine;

public class DweebManagerScript : MonoBehaviour
{
	public GameObject ResultsScreen;
	public GameObject TableDweebCup;
	public GameObject PlayerCharacter;
	public Renderer PlayerMouth;
	public Transform PlayerHand;
	public GameObject OpponentCharacter;
	public Renderer OpponentMouth;
	public Transform OpponentHand;
	public Renderer Eyes;
	public Renderer Mouth;
	public GameObject DweebCup;
	public string OpponentDweebDrink;
	public string PlayerDweebDrink;
	public bool AnimateMouth;
	public bool Animate;
	public GameObject GameOver;
}
