using UnityEngine;

public class LandscapeManagerScript : MonoBehaviour
{
	public CWLandscapeCardDragOld[] CardScripts;
	public GameObject ReadyButton;
	public GameObject HexGrid1;
	public GameObject HexGrid2;
	public GameObject[] player1LandscapeLanes;
	public GameObject[] player2LandscapeLanes;
	public GameObject[] corns;
	public GameObject[] plains;
	public GameObject[] cottons;
	public GameObject[] sands;
	public GameObject[] swamps;
	public int populatedLandscapeCount;
	public GameObject[] landDamageFXprefabs;
}
