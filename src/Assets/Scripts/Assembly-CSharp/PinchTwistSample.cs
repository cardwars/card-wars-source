using UnityEngine;

public class PinchTwistSample : SampleBase
{
	public Transform target;
	public Material twistMaterial;
	public Material pinchMaterial;
	public Material pinchAndTwistMaterial;
	public float pinchScaleFactor;
}
