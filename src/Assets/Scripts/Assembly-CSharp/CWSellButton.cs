using UnityEngine;

public class CWSellButton : MonoBehaviour
{
	public UILabel errorText;
	public UIButtonTween showError;
	public UIButtonTween showConfirm;
	public GameObject tableObject;
	public UILabel CoinLabel;
	public UILabel ConfirmLabel;
	public CWDeckCardList SelectList;
	public UITweener canSellTween;
}
