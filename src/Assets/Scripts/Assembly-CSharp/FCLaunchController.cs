using UnityEngine;

public class FCLaunchController : MonoBehaviour
{
	public GameObject RootUGUI;
	public GameObject UpsellPrefab;
	public GameObject enterMapEvents;
	public Animation battleMapAnimation;
	public string animationIdle;
	public string animationActivate;
	public string loadingScreenTextureName;
}
