using Multiplayer;

public class LevelUpUIScript : AsyncData<ResponseFlag>
{
	public UILabel BannerLabel;
	public UILabel LevelLabel;
	public UILabel DeckLabel;
	public UILabel HPLabel;
	public UILabel PrefaceLabel;
	public CWBattleEndWinnerStats battleEndWinner;
}
