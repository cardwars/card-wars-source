using UnityEngine;

public class UserInfo : MonoBehaviour
{
	public UILabel UserID;
	public UILabel ManifestVersion;
	public UILabel Server;
	public UILabel ClientVersion;
	public UILabel BundleID;
	public UILabel PlayerID;
}
