using UnityEngine;

public class ResultsDweebCardScript : MonoBehaviour
{
	public Vector3 Destination;
	public UISprite DweebSprite;
	public UILabel NameLabel;
	public UILabel DescLabel;
	public float Scale;
	public bool Unlock;
	public bool Grow;
	public AudioClip CardMove;
}
