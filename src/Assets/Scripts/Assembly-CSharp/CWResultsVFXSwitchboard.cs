using UnityEngine;

public class CWResultsVFXSwitchboard : MonoBehaviour
{
	public GameObject RarityVFX_Low;
	public GameObject RarityVFX_Med;
	public GameObject RarityVFX_High;
	public TriggerVFX TriggerScript;
	public TweenPosition ParentTween;
}
