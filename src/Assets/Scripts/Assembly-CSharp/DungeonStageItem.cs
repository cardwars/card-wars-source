using UnityEngine;

public class DungeonStageItem : MonoBehaviour
{
	public GameObject ButtonStart;
	public GameObject WidgetHighlight;
	public GameObject WidgetBlackout;
	public GameObject ButtonLocked;
	public UILabel LabelGemCost;
	public UILabel LabelIndex;
	public UILabel LabelStatus;
	public UISprite Icon;
	public AudioClip LockedAudio;
	public AudioClip UnlockedAudio;
}
