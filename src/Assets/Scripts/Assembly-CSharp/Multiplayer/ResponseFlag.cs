namespace Multiplayer
{
	public enum ResponseFlag
	{
		Success = 0,
		None = 1,
		Invalid = 2,
		Retry = 3,
		Error = 4,
	}
}
