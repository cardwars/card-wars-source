namespace Multiplayer
{
	public class MultiplayerData
	{
		public string name;
		public string icon;
		public string leader;
		public int leaderLevel;
		public int trophies;
		public bool rename;
	}
}
