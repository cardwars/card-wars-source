namespace Multiplayer
{
	public class LeaderboardData
	{
		public int rank;
		public int previousRank;
		public string name;
		public string icon;
		public string leader;
		public int leaderLevel;
		public int trophies;
		public int wins;
		public int losses;
	}
}
