using UnityEngine;

public class LogoPanelScript : MonoBehaviour
{
	public UITexture CNLogo;
	public UITexture D3Logo;
	public UITexture KFFLogo;
	public UITexture Loading;
	public UITexture Background;
	public TweenColor backGroundTween;
	public bool IsComplete;
}
