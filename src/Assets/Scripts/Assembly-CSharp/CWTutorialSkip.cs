using UnityEngine;

public class CWTutorialSkip : MonoBehaviour
{
	public bool leaveCollider;
	public bool useOnClick;
	public string tutorial_ID;
	public bool destroyWhenDone;
	public bool removeInputEnablerWhenDone;
	public Component[] componentsToEnable;
	public bool useInputEnablerWhenDone;
}
