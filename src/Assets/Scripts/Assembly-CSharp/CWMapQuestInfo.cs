using UnityEngine;

public class CWMapQuestInfo : MonoBehaviour
{
	public ScreenDimmer questMapDimmer;
	public GameObject CloseButton;
	public GameObject RewardsUnknown;
	public GameObject[] DeckCards;
}
