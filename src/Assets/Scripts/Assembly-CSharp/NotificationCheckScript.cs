using UnityEngine;

public class NotificationCheckScript : MonoBehaviour
{
	public UITweener partyNotification;
	public GameObject showNotificationsObj;
	public GameObject hideNotificationsObj;
	public UILabel partyTitle;
	public UILabel partyDescription;
	public UILabel expiration;
}
