using UnityEngine;

public class VOController : MonoBehaviour
{
	public AudioClip[] OpeningClips;
	public AudioClip[] WinClips;
	public AudioClip[] LoseClips;
	public AudioClip[] DestroyCreatureClips;
	public AudioClip[] PlayCreatureClips;
	public AudioClip[] PlayBuildingClips;
	public AudioClip[] PlaySpellClips;
	public AudioClip[] CreatureDestroyedClips;
	public AudioClip[] BuildingDestroyedClips;
	public AudioClip[] FloopClips;
	public AudioClip[] HeroDamagedClips;
	public AudioClip[] LeaderAbilityClips;
}
