using UnityEngine;

internal class CreditsUI : MonoBehaviour
{
	public float FIRST_ITEM_Y;
	public UILabel creditLabel;
	public UIDraggablePanel creditsLabelParent;
	public float scrollSpeed;
}
