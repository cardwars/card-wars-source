using System;
using UnityEngine;

[Serializable]
public class ScrollingTextureUVs : MonoBehaviour
{
	public float animationRate;
	public float animationRate_y;
	public Vector2 offset;
	public int direction;
	public int direction_y;
}
