using System;

[Serializable]
public class KFFDictionaryEntry
{
	public string key;
	public string value;
}
