using UnityEngine;

public class DragGesture : ContinuousGesture
{
	public Vector2 LastPos;
	public Vector2 LastDelta;
}
