public class GPGTurnBasedMatch
{
	public bool canRematch;
	public string matchDescription;
	public string matchId;
	public int matchNumber;
	public int matchVersion;
	public string pendingParticipantId;
	public string localParticipantId;
	public int statusInt;
	public int userMatchStatusInt;
	public int availableAutoMatchSlots;
}
