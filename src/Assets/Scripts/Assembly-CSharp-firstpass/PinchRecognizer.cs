public class PinchRecognizer : ContinuousGestureRecognizer<PinchGesture>
{
	public float MinDOT;
	public float MinDistance;
	public float DeltaScale;
}
