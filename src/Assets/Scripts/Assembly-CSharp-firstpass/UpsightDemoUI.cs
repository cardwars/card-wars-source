using UnityEngine;

public class UpsightDemoUI : MonoBehaviour
{
	public string androidAppToken;
	public string androidAppSecret;
	public string gcmProjectNumber;
	public string iosAppToken;
	public string iosAppSecret;
}
