using UnityEngine;

public class UpsightMonoBehavior : MonoBehaviour
{
	public string androidAppToken;
	public string androidAppSecret;
	public string gcmProjectNumber;
	public string iosAppToken;
	public string iosAppSecret;
	public bool registerForPushNotifications;
}
