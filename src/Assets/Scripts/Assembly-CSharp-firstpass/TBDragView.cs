using UnityEngine;

public class TBDragView : MonoBehaviour
{
	public bool allowUserInput;
	public float sensitivity;
	public float dragAcceleration;
	public float dragDeceleration;
	public bool reverseControls;
	public float minPitchAngle;
	public float maxPitchAngle;
	public float idealRotationSmoothingSpeed;
}
