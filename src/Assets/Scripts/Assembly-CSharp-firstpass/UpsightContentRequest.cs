using UnityEngine;

public class UpsightContentRequest : MonoBehaviour
{
	public string placementID;
	public bool showsOverlayImmediately;
	public bool shouldAnimate;
}
