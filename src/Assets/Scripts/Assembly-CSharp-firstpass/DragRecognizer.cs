public class DragRecognizer : ContinuousGestureRecognizer<DragGesture>
{
	public float MoveTolerance;
	public bool ApplySameDirectionConstraint;
}
