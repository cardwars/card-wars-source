public class FingerMotionDetector : FingerEventDetector<FingerMotionEvent>
{
	public string MoveMessageName;
	public string StationaryMessageName;
	public bool TrackMove;
	public bool TrackStationary;
}
