public class LongPressRecognizer : DiscreteGestureRecognizer<LongPressGesture>
{
	public float Duration;
	public float MoveTolerance;
}
