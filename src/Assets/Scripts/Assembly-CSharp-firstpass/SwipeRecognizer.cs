public class SwipeRecognizer : DiscreteGestureRecognizer<SwipeGesture>
{
	public float MinDistance;
	public float MaxDistance;
	public float MinVelocity;
	public float MaxDeviation;
}
