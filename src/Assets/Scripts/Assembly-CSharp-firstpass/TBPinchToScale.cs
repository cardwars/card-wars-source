using UnityEngine;

public class TBPinchToScale : MonoBehaviour
{
	public Vector3 scaleWeights;
	public float minScaleAmount;
	public float maxScaleAmount;
	public float sensitivity;
	public float smoothingSpeed;
}
