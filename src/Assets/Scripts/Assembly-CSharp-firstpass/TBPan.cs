using UnityEngine;

public class TBPan : MonoBehaviour
{
	public float sensitivity;
	public float smoothSpeed;
	public BoxCollider moveArea;
	public Vector3 idealPos;
}
