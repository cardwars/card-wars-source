# Card Wars TCG App Unity Source

This was extracted and compiled by me. I hope to keep the game alive by eventually fixing the broken source (though this might be nearly impossible with the Unity knowledge I have). If anyone wants to contribute please go ahead! Don't let the game die! Maybe we'll be able to port it to PC/Browser and get our own server up sometime.

https://discord.com/invite/pH2AS45

### Special Notes:

1. The game was developed in Unity version `5.2.3f1` but the software I used to re-assemble/decompile it turned it into the version it currently is in (check source I forget what version it was I'm just uploading this like 5 months from when I did all of this lol).
2. The server address used to be `app-lb-group.floop-kff.com` . IF only we could go back in time to reverse the methods on how it worked, I bet it wouldn't even be that hard.
3. There is a total of 595 cards in this game (Though I wonder how many are obtainable).